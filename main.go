package main

import (
	"fmt"
	"net/http"
)

func home(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "<h1>Hola Mundo</h1>")
}
func prueba(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "<h1>Hola Mundo desde la página de prueba</h1>")
}

func usuario (w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "<h1>Hola Usuario</h1>")
}

func main() {
	//Varias rutas
	mux := http.NewServeMux()
	//ruta / inicio
	mux.HandleFunc("/", home)
	// ruta /prueba pagina de prueba
	mux.HandleFunc("/prueba", prueba)
	// ruta /usuarios paginas de usuario
	mux.HandleFunc("/usuario", usuario)
	// Crear el servidor
	http.ListenAndServe(":8080", mux)
}
